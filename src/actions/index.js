export const exampleAction = payload => async dispatch => {
  setTimeout(() => {
    console.log(payload)
    const val = payload.target.value
    dispatch(asyncSuccess(val))
  }, 600)
}

const asyncSuccess = payload => ({
  type: "ASYNC_SUCCESS",
  payload
})
