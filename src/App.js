import React from "react"
import { connect } from "react-redux"
import "./App.css"
import { exampleAction } from "./actions/"

export const App = props => {
  return (
    <div className="container">
      <h1 className="text-4xl">App</h1>
      <label>Type:</label>
      <input
        onChange={e => {
          e.persist()
          props.exampleAction(e)
        }}
        type="text"
      />
      <p>
        Val: <span>{props.appState.val}</span>
      </p>
    </div>
  )
}

const mapStateToProps = state => {
  console.log("props-state", state)
  return { ...state }
}

const mapDispatchToProps = dispatch => ({
  exampleAction: payload => dispatch(exampleAction(payload))
  // Map all additional actions here
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
