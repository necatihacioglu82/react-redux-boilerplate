export default (state = {}, action) => {
  switch (action.type) {
    case "ASYNC_SUCCESS":
      console.log("action in reducer", state, action, action.payload)
      return Object.assign({ loading: false }, { val: action.payload })
    default:
      return state
  }
}
